# WWU Drupal Docker

Configuration for running our Drupal profile in a container.

## Installing Docker Compose

See: [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/#set-up-the-repository)

## Building the Container Images

* Build with Docker Compose
`docker compose build`

* Test the build by starting the app
`docker compose up`

* Clean containers (after stopping)
`docker container prune`

## Pushing Container Images to Azure Container Registry

See: [Push your first image to your Azure container registry using the Docker CLI](https://learn.microsoft.com/en-us/azure/container-registry/container-registry-get-started-docker-cli)

* Log in to Azure and the wwuweb container registry
`az login`
`az acr login --name wwuweb`
* Build the image
`docker compose build`
* Alias the image with the fully qualified path to the registry
`docker tag wwu_drupal_docker-web:<TAG> wwuweb.azurecr.io/wwu_drupal_docker-web`
* Push the image
`docker push wwuweb.azurecr.io/wwu_drupal_docker-web`

## Reverse Proxy to Container Via VM

Assuming a locally-hosted VM with a locally-hosted DNS server configured to direct the "local" domain to the VM:

```
<VirtualHost *:80>

        ServerName <domain>.local

        <IfModule mod_proxy_http.c>
                ProxyVia On
                ProxyPass / http://localhost:8080/
                ProxyPassReverse / http://localhost:8080/
        </IfModule>

</VirtualHost>
```

Replace `<domain>` above with the desired local domain for the containerized site.

## Execute Command in the Web Container

`docker exec -it wwu_drupal_docker-web-1 <command>`

Where `<command>` is the desired command. Change the container name as needed to address the database or if there are multiple instances of the containers running (i.e. web-1 would become web-2 and so on).

## List all web apps in a resource group

`az webapp list --resource-group <group>`
