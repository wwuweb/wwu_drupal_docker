#!/bin/bash
set -e

# Execute the main command passed as arguments.
exec "$@"
