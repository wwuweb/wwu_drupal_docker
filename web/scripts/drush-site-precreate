#!/bin/bash
set -e

# Check if Drupal is bootstrapped.
drupal_bootstrapped()
{
  drush --yes core:status --field=bootstrap | grep -q Successful
}

# Install the Drupal site.
drush_site_install()
{
  set -x
  drush \
    --yes \
    -vv \
    site:install \
    ${DRUPAL_INSTALL_PROFILE} \
    --site-name=${DRUPAL_SITE_NAME} \
    --account-name=${DRUPAL_ACCOUNT_NAME} \
    --account-pass=${DRUPAL_ACCOUNT_PASSWORD} \
    --db-url=mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}
}

# Apply Drupal database updates.
drush_updatedb()
{
  set -x
  drush \
    --yes \
    -vv \
    updatedb \
    --cache-clear=0 \
    --post-updates=1; \
}

# Rebuild the Drupal site cache.
drush_cache_rebuild()
{
  set -x
  drush \
    --yes \
    -vv \
    cache:rebuild
}

if [ -d "/var/webapp/drupal/sites" ]; then
  rm -rf /opt/drupal/web/sites
else
  mv /opt/drupal/web/sites /var/webapp/drupal
fi

ln -s /var/webapp/drupal/sites /opt/drupal/web/sites

chown --no-dereference www-data:www-data \
  /opt/drupal/web/sites \
  /var/webapp/drupal/sites

# Start the SSH service.
service ssh start

# Wait for mysql server to be available.
wait-for-mysql

if ! drupal_bootstrapped; then
  # Drupal is not boostrapped, so install it.
  drush_site_install
else
  # Drupal is bootstrapped, so run database updates and clear caches.
  drush_updatedb
  drush_cache_rebuild
fi

# Start the Apache webserver.
exec apache2-foreground
